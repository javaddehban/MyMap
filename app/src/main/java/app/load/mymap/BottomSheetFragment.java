package app.load.mymap;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.willy.ratingbar.ScaleRatingBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class BottomSheetFragment extends BottomSheetDialogFragment {

    private List<Markers> list;
    TextView title;
    String sTitle;
    Button btn_quality, btn_rating, btn_closest;

    OnClickPlace onClickPlace;

    BottomSheetFragment(String title,OnClickPlace onClickPlace) {
        this.sTitle = title;
        this.onClickPlace = onClickPlace;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_button_sheet,
                container,
                false);
        title = view.findViewById(R.id.title);
        btn_quality = view.findViewById(R.id.btn_quality);
        btn_rating = view.findViewById(R.id.btn_rating);
        btn_closest = view.findViewById(R.id.btn_closest);

        title.setText(sTitle);

        btn_quality.setOnClickListener(view1 -> onClickPlace.onclick("quality"));

        btn_closest.setOnClickListener(view12 -> onClickPlace.onclick("closest"));

        btn_rating.setOnClickListener(view13 -> onClickPlace.onclick("rating"));
//        AdapterDialogAccountChild adapter = new AdapterDialogAccountChild(getContext());
//        RecyclerView recyclerView = view.findViewById(R.id.recyclerDialogAccount);
//        recyclerView.setLayoutManager
//                (new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//        recyclerView.setAdapter(adapter);

        return view;

    }


    public class AdapterDialogAccountChild extends RecyclerView.Adapter<AdapterDialogAccountChild.ViewHolder> {

        Context mContext;

        AdapterDialogAccountChild(Context mContext) {
            this.mContext = mContext;
        }

        @NonNull
        @Override
        public AdapterDialogAccountChild.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sample_marker, viewGroup, false);

            return new AdapterDialogAccountChild.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final AdapterDialogAccountChild.ViewHolder viewHolder, final int position) {

            viewHolder.setIsRecyclable(false);
            viewHolder.Text.setText(list.get(position).tag);
            viewHolder.description.setText(list.get(position).address);
            viewHolder.ratingBar.setRating(list.get(position).star);
            viewHolder.ratingBar.setClickable(false);
            viewHolder.ratingBar.setScrollable(false);
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            private TextView Text, description;
            ScaleRatingBar ratingBar;


            ViewHolder(@NonNull View itemView) {
                super(itemView);

                Text = itemView.findViewById(R.id.text_marker);
                description = itemView.findViewById(R.id.text_description);
                ratingBar = itemView.findViewById(R.id.simpleRatingBar);
            }
        }
    }


}