package app.load.mymap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import android.Manifest;
import android.app.Dialog;

import android.app.DialogFragment;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.OnMapReadyCallback;
//import com.google.android.gms.maps.SupportMapFragment;
//import com.google.android.gms.maps.model.BitmapDescriptorFactory;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.Marker;
//import com.google.android.gms.maps.model.MarkerOptions;
//import com.google.android.gms.maps.model.PolygonOptions;
//import com.google.android.gms.maps.model.Polyline;
//import com.google.android.gms.maps.model.PolylineOptions;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.directions.v5.DirectionsCriteria;
import com.mapbox.api.directions.v5.MapboxDirections;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.BubbleLayout;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonOptions;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.mapboxsdk.utils.BitmapUtils;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.graphics.Color.parseColor;
import static com.mapbox.core.constants.Constants.PRECISION_6;
import static com.mapbox.mapboxsdk.style.expressions.Expression.color;
import static com.mapbox.mapboxsdk.style.expressions.Expression.interpolate;
import static com.mapbox.mapboxsdk.style.expressions.Expression.lineProgress;
import static com.mapbox.mapboxsdk.style.expressions.Expression.linear;
import static com.mapbox.mapboxsdk.style.expressions.Expression.literal;
import static com.mapbox.mapboxsdk.style.expressions.Expression.match;
import static com.mapbox.mapboxsdk.style.expressions.Expression.stop;
import static com.mapbox.mapboxsdk.style.layers.Property.ICON_ANCHOR_BOTTOM;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAnchor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconOffset;

import static com.mapbox.mapboxsdk.style.expressions.Expression.eq;
import static com.mapbox.mapboxsdk.style.expressions.Expression.get;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineCap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineGradient;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineJoin;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineWidth;

public class MapsActivity extends AppCompatActivity
        implements OnMapReadyCallback,
        MapboxMap.OnMapClickListener,
        PermissionsListener,
        OnClickPlace {

    List<Markers> cafe = new ArrayList<>();
    List<Markers> cinema = new ArrayList<>();
    List<Markers> restaurant = new ArrayList<>();
    List<Markers> park = new ArrayList<>();

    LinearLayout btn_cafe, btn_cinema, btn_restaurant, btn_park;
    ImageView img_location;
    //    LatLng currentLatLng;
    LatLng currentLatLng;

    //    private GoogleMap mMap;
    AlertDialog optionDialog;
    private BottomSheetFragment bottomSheetFragment;

    MarkerOptions markerOptions = null;
    Marker markerCurrent = null;

    private MapView mapView;
    private MapboxMap mapboxMap;

    LocationManager lm;
    ImageView about;


    private static final String GEOJSON_SOURCE_ID = "GEOJSON_SOURCE_ID";
    private static final String MARKER_IMAGE_ID = "MARKER_IMAGE_ID";
    private static final String MARKER_LAYER_ID = "MARKER_LAYER_ID";
    private static final String CALLOUT_LAYER_ID = "CALLOUT_LAYER_ID";
    private static final String PROPERTY_SELECTED = "selected";
    private static final String PROPERTY_NAME = "name";
    private static final String PROPERTY_CAPITAL = "capital";
    private static final String TAG = "Mapssss";

    private GeoJsonSource source;
    private FeatureCollection featureCollection;

    private PermissionsManager permissionsManager;


    Point origin;
    Point destination;
    private DirectionsRoute currentRoute;
    private MapboxDirections client;

    private static final String ORIGIN_ICON_ID = "origin-icon-id";
    private static final String DESTINATION_ICON_ID = "destination-icon-id";
    private static final String ROUTE_LAYER_ID = "route-layer-id";
    private static final String ROUTE_LINE_SOURCE_ID = "route-source-id";
    private static final String ICON_LAYER_ID = "icon-layer-id";
    private static final String ICON_SOURCE_ID = "icon-source-id";

    private static Point ORIGIN_POINT = Point.fromLngLat(36.28094665090292, 50.013909529681314);
    private static Point DESTINATION_POINT = Point.fromLngLat(36.283952666180824, 50.00497557326912);

    private static final String ROUTE_SOURCE_ID = "1";
    private static final String RED_PIN_ICON_ID = "red-pin-icon-id";
    int count = 0;
    LineLayer routeLayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setContentView(R.layout.activity_maps);

//****************************************** Add Marker ***************************************************************
        cafe.add(new Markers(1, 36.28260730172687, 50.013909529681314, "کافه آدم و حوا", "قزوین", 2, R.drawable.ic_cafe));
        cafe.add(new Markers(2, 36.283952666180824, 50.00497557326912, "hi", "qazvin", 3, R.drawable.ic_cafe));
//        36.256137  50.004585
        restaurant.add(new Markers(3, 36.256137, 50.004585, "رستوران نمونه", " قزوین، خیابان بوعلی تقاطع فردوسی", 5, R.drawable.ress));
        restaurant.add(new Markers(4, 36.265461, 49.993462, "رستوران شاندیز", "خیابان مصیب مرادی", 1, R.drawable.ress));
        restaurant.add(new Markers(9, 36.317449, 50.00497557326912, "رستوران شمس", "خیابان مولوی", 3, R.drawable.ress));
        restaurant.add(new Markers(10, 36.317449, 50.00497557326912, "رستوران ارکیده", "خیابان نوروزیان", 1, R.drawable.ress));
        restaurant.add(new Markers(11, 36.314323, 50.026576, "رستوران ته چین", "خیابان پرستار", 5, R.drawable.ress));
        restaurant.add(new Markers(12, 36.307199, 49.995858, "رستوران کرمانی", "خیابان ولیعصر", 4, R.drawable.ress));
        restaurant.add(new Markers(13, 36.267072, 50.019969, "رستوران اقبالی", "خیابان خیام", 2, R.drawable.ress));

        cinema.add(new Markers(5, 36.265931, 49.991452, "سینما مهتاب", "tabriz", 2, R.drawable.ic_cinema));
        cinema.add(new Markers(6, 36.215421, 49.993462, "سینما بهمن", "mashhad", 4, R.drawable.ic_cinema));

        park.add(new Markers(7, 36.255431, 49.991452, "پارک دهخدا", "kerman", 1, R.drawable.ic_park));
        park.add(new Markers(8, 36.265421, 49.993462, "پارک الغذیر", "shiraz", 2, R.drawable.ic_park));

//****************************************** Add Marker ***************************************************************
        currentLatLng = new LatLng(36.288107, 50.007008);


//****************************************** Init Google Map ***************************************************************
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);
        Mapbox.getInstance(this, "pk.eyJ1IjoiamF2YWRhcyIsImEiOiJjamtiN3NmaDcwMWxyM3ZxbmV5MzQ5MWx1In0.iDQX4oJ06TRCyc2vS1pvqw");
        setContentView(R.layout.activity_maps);
        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
//****************************************** Find View By Id ***************************************************************

        btn_cafe = findViewById(R.id.btn_cofe);
        btn_cinema = findViewById(R.id.btn_cinama);
        btn_restaurant = findViewById(R.id.btn_resturan);
        btn_park = findViewById(R.id.btn_park);
        img_location = findViewById(R.id.img_location);
        about = findViewById(R.id.about);

//****************************************** Call Permission ***************************************************************
        ActivityCompat.requestPermissions(MapsActivity.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                1);

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Dialog dialog = new Dialog(MapsActivity.this);
//                dialog.setContentView(R.layout.dialog_about);
                optionDialog = new AlertDialog.Builder(MapsActivity.this).create();
                View viewAbot = getLayoutInflater().inflate(R.layout.dialog_about, null);
                optionDialog.setView(viewAbot);

                optionDialog.show();
            }
        });
        img_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                getMyLocation();
                mapboxMap.getStyle(new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        enableLocationComponent(style);
                    }
                });
            }
        });
    }

    private void initSource(@NonNull Style loadedMapStyle) {
        loadedMapStyle.addSource(new GeoJsonSource(ROUTE_SOURCE_ID + count,
                FeatureCollection.fromFeatures(new Feature[]{})));

        GeoJsonSource iconGeoJsonSource = new GeoJsonSource(ICON_SOURCE_ID + count, FeatureCollection.fromFeatures(new Feature[]{
                Feature.fromGeometry(Point.fromLngLat(origin.longitude(), origin.latitude())),
                Feature.fromGeometry(Point.fromLngLat(destination.longitude(), destination.latitude()))}));
        loadedMapStyle.addSource(iconGeoJsonSource);
    }

    /**
     * Add the route and marker icon layers to the map
     */
    private void initLayers(@NonNull Style loadedMapStyle) {
        if (routeLayer != null) {
            loadedMapStyle.removeLayer(routeLayer);
//            loadedMapStyle.removeImage(RED_PIN_ICON_ID);
        }
        routeLayer = new LineLayer(ROUTE_LAYER_ID + count, ROUTE_SOURCE_ID + count);
// Add the LineLayer to the map. This layer will display the directions route.
        routeLayer.setProperties(
                lineCap(Property.LINE_CAP_ROUND),
                lineJoin(Property.LINE_JOIN_ROUND),
                lineWidth(5f),
                lineColor(Color.parseColor("#009688"))
        );
        loadedMapStyle.addLayer(routeLayer);

// Add the red marker icon image to the map
//        loadedMapStyle.addImage(RED_PIN_ICON_ID, BitmapUtils.getBitmapFromDrawable(
//                getResources().getDrawable(R.drawable.ic_marker_red)));
//
// Add the red marker icon SymbolLayer to the map
        loadedMapStyle.addLayer(new SymbolLayer(ICON_LAYER_ID + count, ICON_SOURCE_ID + count).withProperties(
                iconImage(RED_PIN_ICON_ID),
                iconIgnorePlacement(true),
                iconAllowOverlap(true),
                iconOffset(new Float[]{0f, -9f})));
    }

    /**
     * Make a request to the Mapbox Directions API. Once successful, pass the route to the
     * route layer.
     *
     * @param mapboxMap   the Mapbox map object that the route will be drawn on
     * @param origin      the starting point of the route
     * @param destination the desired finish point of the route
     */
    private void getRoute(MapboxMap mapboxMap, Point origin, Point destination) {
        client = MapboxDirections.builder()
                .origin(origin)
                .destination(destination)
                .overview(DirectionsCriteria.OVERVIEW_FULL)
                .profile(DirectionsCriteria.PROFILE_DRIVING)
                .accessToken(getString(R.string.mapbox_access_token))
                .build();

        client.enqueueCall(new Callback<DirectionsResponse>() {
            @Override
            public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
// You can get the generic HTTP info about the response
//                Timber.d("Response code: " + response.code());
                Log.e(TAG, "onResponse: " + response.code());
                if (response.body() == null) {
//                    Timber.e("No routes found, make sure you set the right user and access token.");
                    Log.e(TAG, "onResponse: No routes found, make sure you set the right user and access token.");
                    return;
                } else if (response.body().routes().size() < 1) {
//                    Timber.e("No routes found");
                    Log.e(TAG, "onResponse: No routes found");
                    return;
                }

// Get the directions route
                currentRoute = response.body().routes().get(0);

// Make a toast which displays the route's distance
//                Toast.makeText(MapsActivity.this, String.format(
//                        getString(R.string.directions_activity_toast_message),
//                        currentRoute.distance()), Toast.LENGTH_SHORT).show();

                if (mapboxMap != null) {
                    mapboxMap.getStyle(new Style.OnStyleLoaded() {
                        @Override
                        public void onStyleLoaded(@NonNull Style style) {

// Retrieve and update the source designated for showing the directions route
                            GeoJsonSource source = style.getSourceAs(ROUTE_SOURCE_ID + count);

// Create a LineString with the directions route's geometry and
// reset the GeoJSON source for the route LineLayer source
                            if (source != null) {
//                                Timber.d("onResponse: source != null");
                                Log.e(TAG, "onStyleLoaded: source != null");
                                source.setGeoJson(FeatureCollection.fromFeature(
                                        Feature.fromGeometry(LineString.fromPolyline(currentRoute.geometry(), PRECISION_6))));
                                count++;
//                                mapboxMap.remove
                            }
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
//                Timber.e("Error: " + throwable.getMessage());
                Log.e(TAG, "onFailure: " + throwable.getMessage());
//                Toast.makeText(DirectionsActivity.this, "Error: " + throwable.getMessage(),
//                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(@NonNull Style style) {
//                new LoadGeoJsonDataTask(MapsActivity.this).execute();

                mapboxMap.setOnMarkerClickListener(new MapboxMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(final Marker marker) {

                        if (marker.getTitle() != null) {
                            destination = Point.fromLngLat(marker.getPosition().getLatitude(), marker.getPosition().getLongitude());
                            optionDialog = new AlertDialog.Builder(MapsActivity.this).create();
                            ImageView imageView;
                            TextView text_name, text_address;
                            Button btn_go;
                            final View view = getLayoutInflater().inflate(R.layout.dialog_show_detail, null);
                            optionDialog.setView(view);

                            imageView = view.findViewById(R.id.image_place);
                            text_name = view.findViewById(R.id.text_name);
                            text_address = view.findViewById(R.id.text_address);
                            btn_go = view.findViewById(R.id.btn_go);

                            text_name.setText(marker.getTitle());
                            text_address.setText(marker.getSnippet());
//                            imageView.setImageResource((int) marker.getTag());


                            btn_go.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // TODO: 12/20/19 direction

//                                    origin = Point.fromLngLat(40.660636, -107.649042);
//                                    destination = Point.fromLngLat(35.374717, -95.823698);


//                                    DESTINATION_POINT = Point.fromLngLat(marker.getPosition().getLongitude(), marker.getPosition().getLatitude());
//                                    ORIGIN_POINT = Point.fromLngLat(cafe.get(1).getLat(), cafe.get(1).getLng());
//                                    DESTINATION_POINT = Point.fromLngLat(marker.getPosition().getLatitude(), marker.getPosition().getLongitude());
//                                    initSources(style);
//                                    initLayers(style);
////                                    :51.40818816233991,"lat":35.71895997864034
//                                    ORIGIN_POINT = Point.fromLngLat(35.713684939821604,51.36863572808829);
//// Get the directions route from the Mapbox Directions API
//                                    getRoute(mapboxMap, ORIGIN_POINT, DESTINATION_POINT);
//                                    mapboxMap.
                                    LocationComponent component = mapboxMap.getLocationComponent();

                                    origin = Point.fromLngLat(component.getLastKnownLocation().getLongitude(), component.getLastKnownLocation().getLatitude());

// Set the destination location to the Plaza del Triunfo in Granada, Spain.
                                    destination = Point.fromLngLat(marker.getPosition().getLongitude(), marker.getPosition().getLatitude());

                                    initSource(style);

                                    initLayers(style);

// Get the directions route from the Mapbox Directions API
                                    getRoute(mapboxMap, origin, destination);

// Get a new Directions API route to that new destination and eventually re-draw the
// gradient route line.
//                                    getRoute(mapboxMap, ORIGIN_POINT, DESTINATION_POINT);

//                                    Toast.makeText(MapsActivity.this,
//                                            getString(R.string.tap_map_instruction_gradient_directions), Toast.LENGTH_SHORT).show();

//                                    MapboxDirections client = MapboxDirections.builder()
//                                            .origin(origin)
//                                            .destination(destination)
//                                            .overview(DirectionsCriteria.OVERVIEW_FULL)
//                                            .profile(DirectionsCriteria.PROFILE_DRIVING)
//                                            .accessToken(getResources().getString(R.string.mapbox_access_token))
//                                            .build();
//                                    List<Marker> listMarkers = new ArrayList<>();
//                                    List<LatLng> listLatLngs = new ArrayList<>();
//
//                                    if (currentLatLng == null) {
//                                        Toast.makeText(MapsActivity.this, "لوکیشن خود را بررسی کنید", Toast.LENGTH_SHORT).show();
//                                        return;
//                                    }

//                                    markerOptions = new MarkerOptions().position(currentLatLng);
//                                    markerCurrent = mapboxMap.addMarker(markerOptions);
//
//                                    listLatLngs.add(marker.getPosition());
//                                    listLatLngs.add(markerOptions.getPosition());
//                                    listMarkers.add(marker);
//                                    listMarkers.add(markerCurrent);
//                                    Polyline polyline = null;
//                                    if (polyline != null)
//                                        polyline.remove();
//
//
//                                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
//
//                                    PolylineOptions polylineOptions = new PolylineOptions().addAll(listLatLngs).clickable(true);
//                                    googleMap.addPolyline(polylineOptions);


//                            GoogleDirection.withServerKey("AIzaSyCliyo6n938nEvYG63UeDrolHFA4iOgIaI")
//                                    .from(currentLatLng)
//                                    .to(marker.getPosition())
//                                    .avoid(AvoidType.FERRIES)
//                                    .avoid(AvoidType.HIGHWAYS)
//                                    .execute(new DirectionCallback() {
//                                        @Override
//                                        public void onDirectionSuccess(Direction direction) {
//                                            if(direction.isOK()) {
//                                                // Do something
//                                                Toast.makeText(MapsActivity.this, "ok", Toast.LENGTH_SHORT).show();
//
//                                            } else {
//                                                // Do something
//                                                Toast.makeText(MapsActivity.this, "no", Toast.LENGTH_SHORT).show();
//
//                                            }
//                                        }
//
//                                        @Override
//                                        public void onDirectionFailure(Throwable t) {
//                                            // Do something
//                                            Toast.makeText(MapsActivity.this, "failure", Toast.LENGTH_SHORT).show();
//                                        }
//                                    });

//                            googleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(currentLatLng.latitude, currentLatLng.longitude))
//                                    .title("مبدا")
//                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
//
//                            // Getting URL to the Google Directions API
//                            String url = getDirectionsUrl(currentLatLng, marker.getPosition());
//
//                            DownloadTask downloadTask = new DownloadTask();
//
//                            // Start downloading json data from Google Directions API
//                            downloadTask.execute(url);
//                                    new FetchURL(MapsActivity.this).execute(getUrl(currentLatLng, marker.getPosition(), "driving"), "driving");
                                    optionDialog.dismiss();
                                }
                            });


                            optionDialog.show();
                        }

                        return false;
                    }
                });
//


                btn_cinema.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mapboxMap.clear();
//                        IconFactory iconFactory = IconFactory.getInstance(MapsActivity.this);
//                        Icon icon = iconFactory.fromResource(R.drawable.ic_cinema);
                        for (int i = 0; i < cinema.size(); i++) {
                            Marker marker =
                                    mapboxMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(cinema.get(i).lat, cinema.get(i).lng))
                                            .title(cinema.get(i).tag)
//                                            .icon(icon)
                                            .snippet(cinema.get(i).address));
//                            marker.setTag(cinema.get(i).image);

                        }
                        mapboxMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(cinema.get(0).lat, cinema.get(0).lng)));
                        bottomSheetFragment = new BottomSheetFragment("سینما", MapsActivity.this::onclick);
                        //noinspection deprecation
                        bottomSheetFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme);
                        bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
                    }
                });
                btn_cafe.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mapboxMap.clear();
                        for (int i = 0; i < cafe.size(); i++) {
                            Marker marker =
                                    mapboxMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(cafe.get(i).lat, cafe.get(i).lng))
                                            .title(cafe.get(i).tag)
//                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                                            .snippet(cafe.get(i).address));
//                    marker.setTag(cafe.get(i).image);
                        }

                        mapboxMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(cafe.get(0).lat, cafe.get(0).lng)));
                        bottomSheetFragment = new BottomSheetFragment("کافی شاپ", MapsActivity.this::onclick);
                        //noinspection deprecation
                        bottomSheetFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme);
                        bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
                    }
                });
                btn_restaurant.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mapboxMap.clear();
                        for (int i = 0; i < restaurant.size(); i++) {
                            Marker marker =
                                    mapboxMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(restaurant.get(i).lat, restaurant.get(i).lng))
                                            .title(restaurant.get(i).tag)
//                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                                            .snippet(restaurant.get(i).address));
//                    marker.setTag(restaurant.get(i).image);
                        }
                        mapboxMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(restaurant.get(0).lat, restaurant.get(0).lng)));
                        bottomSheetFragment = new BottomSheetFragment("رستوران", MapsActivity.this::onclick);
                        //noinspection deprecation
                        bottomSheetFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme);
                        bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
                    }
                });
                btn_park.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mapboxMap.clear();
                        for (int i = 0; i < park.size(); i++) {
                            Marker marker =
                                    mapboxMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(park.get(i).lat, park.get(i).lng))
                                            .title(park.get(i).tag)
//                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                                            .snippet(park.get(i).address));
//                    marker.setTag(park.get(i).image);
                        }
                        mapboxMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(park.get(0).lat, park.get(0).lng)));
                        bottomSheetFragment = new BottomSheetFragment("پارک", MapsActivity.this::onclick);
                        //noinspection deprecation
                        bottomSheetFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme);
                        bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
                    }
                });
                mapboxMap.addOnMapClickListener(MapsActivity.this);
                enableLocationComponent(style);
            }
        });
    }

    @Override
    public void onclick(String tag) {
        Log.e(TAG, "onclick: " + tag);

        switch (tag) {
            case "quality":
                mapboxMap.clear();
                     Marker marker =
                            mapboxMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(restaurant.get(0).lat, restaurant.get(0).lng))
                                    .title(restaurant.get(0).tag)
//                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                                    .snippet(restaurant.get(0).address));
//                    marker.setTag(restaurant.get(i).image);

                mapboxMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(restaurant.get(0).lat, restaurant.get(0).lng)));

                break;
            case "closest":
                mapboxMap.clear();
                Marker marker1 =
                        mapboxMap.addMarker(new MarkerOptions()
                                .position(new LatLng(restaurant.get(1).lat, restaurant.get(1).lng))
                                .title(restaurant.get(1).tag)
//                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                                .snippet(restaurant.get(1).address));
//                    marker.setTag(restaurant.get(i).image);

                mapboxMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(restaurant.get(1).lat, restaurant.get(1).lng)));

                break;
            case "rating":
                mapboxMap.clear();
                Marker marker2 =
                        mapboxMap.addMarker(new MarkerOptions()
                                .position(new LatLng(restaurant.get(2).lat, restaurant.get(2).lng))
                                .title(restaurant.get(2).tag)
//                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                                .snippet(restaurant.get(2).address));
//                    marker.setTag(restaurant.get(i).image);

                mapboxMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(restaurant.get(2).lat, restaurant.get(2).lng)));

                break;
        }
    }


//    public void setUpData(final FeatureCollection collection) {
//        featureCollection = collection;
//        if (mapboxMap != null) {
//            mapboxMap.getStyle(style -> {
//                setupSource(style);
//                setUpImage(style);
//                setUpMarkerLayer(style);
//                setUpInfoWindowLayer(style);
//            });
//        }
//    }

//    /**
//     * Adds the GeoJSON source to the map
//     */
//    private void setupSource(@NonNull Style loadedStyle) {
//        source = new GeoJsonSource(GEOJSON_SOURCE_ID, featureCollection);
//        loadedStyle.addSource(source);
//    }
//
//    /**
//     * Adds the marker image to the map for use as a SymbolLayer icon
//     */
//    private void setUpImage(@NonNull Style loadedStyle) {
//        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_park, null);
//        Bitmap mBitmap = BitmapUtils.getBitmapFromDrawable(drawable);
//
//        loadedStyle.addImage(MARKER_IMAGE_ID, mBitmap);
////                BitmapFactory.decodeResource(
////                MapsActivity.this.getResources(), R.drawable.mapbox_info_bg_selector));
//    }
//
//    /**
//     * Updates the display of data on the map after the FeatureCollection has been modified
//     */
//    private void refreshSource() {
//        if (source != null && featureCollection != null) {
//            source.setGeoJson(featureCollection);
//        }
//    }
//
//    /**
//     * Setup a layer with maki icons, eg. west coast city.
//     */
//    private void setUpMarkerLayer(@NonNull Style loadedStyle) {
//        loadedStyle.addLayer(new SymbolLayer(MARKER_LAYER_ID, GEOJSON_SOURCE_ID)
//                .withProperties(
//                        iconImage(MARKER_IMAGE_ID),
//                        iconAllowOverlap(true),
//                        iconOffset(new Float[]{0f, -8f})
//                ));
//    }
//
//    /**
//     * Setup a layer with Android SDK call-outs
//     * <p>
//     * name of the feature is used as key for the iconImage
//     * </p>
//     */
//    private void setUpInfoWindowLayer(@NonNull Style loadedStyle) {
//        loadedStyle.addLayer(new SymbolLayer(CALLOUT_LAYER_ID, GEOJSON_SOURCE_ID)
//                .withProperties(
//                        /* show image with id title based on the value of the name feature property */
//                        iconImage("{name}"),
//
//                        /* set anchor of icon to bottom-left */
//                        iconAnchor(ICON_ANCHOR_BOTTOM),
//
//                        /* all info window and marker image to appear at the same time*/
//                        iconAllowOverlap(true),
//
//                        /* offset the info window to be above the marker */
//                        iconOffset(new Float[]{-2f, -28f})
//                )
///* add a filter to show only when selected feature property is true */
//                .withFilter(eq((get(PROPERTY_SELECTED)), literal(true))));
//    }

    /**
     * This method handles click events for SymbolLayer symbols.
     * <p>
     * When a SymbolLayer icon is clicked, we moved that feature to the selected state.
     * </p>
     *
     * @param screenPoint the point on screen clicked
     */
    private boolean handleClickIcon(PointF screenPoint) {
        List<Feature> features = mapboxMap.queryRenderedFeatures(screenPoint, MARKER_LAYER_ID);
        if (!features.isEmpty()) {
            String name = features.get(0).getStringProperty(PROPERTY_NAME);
            List<Feature> featureList = featureCollection.features();
            if (featureList != null) {
                for (int i = 0; i < featureList.size(); i++) {
                    if (featureList.get(i).getStringProperty(PROPERTY_NAME).equals(name)) {
                        if (featureSelectStatus(i)) {
                            setFeatureSelectState(featureList.get(i), false);
                        } else {
                            setSelected(i);
                        }
                    }
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Set a feature selected state.
     *
     * @param index the index of selected feature
     */
    private void setSelected(int index) {
        if (featureCollection.features() != null) {
            Feature feature = featureCollection.features().get(index);
            setFeatureSelectState(feature, true);
//            refreshSource();
        }
    }

    /**
     * Selects the state of a feature
     *
     * @param feature the feature to be selected.
     */
    private void setFeatureSelectState(Feature feature, boolean selectedState) {
        if (feature.properties() != null) {
            feature.properties().addProperty(PROPERTY_SELECTED, selectedState);
//            refreshSource();
        }
    }

    /**
     * Checks whether a Feature's boolean "selected" property is true or false
     *
     * @param index the specific Feature's index position in the FeatureCollection's list of Features.
     * @return true if "selected" is true. False if the boolean property is false.
     */
    private boolean featureSelectStatus(int index) {
        if (featureCollection == null) {
            return false;
        }
        return featureCollection.features().get(index).getBooleanProperty(PROPERTY_SELECTED);
    }

    /**
     * Invoked when the bitmaps have been generated from a view.
     */
    public void setImageGenResults(HashMap<String, Bitmap> imageMap) {
        if (mapboxMap != null) {
            mapboxMap.getStyle(style -> {
// calling addImages is faster as separate addImage calls for each bitmap.
                style.addImages(imageMap);
            });
        }
    }

    @Override
    public boolean onMapClick(@NonNull LatLng point) {
        return handleClickIcon(mapboxMap.getProjection().toScreenLocation(point));

    }

    /**
     * AsyncTask to load data from the assets folder.
     */
//    private static class LoadGeoJsonDataTask extends AsyncTask<Void, Void, FeatureCollection> {
//
//        private final WeakReference<MapsActivity> activityRef;
//
//        LoadGeoJsonDataTask(MapsActivity activity) {
//            this.activityRef = new WeakReference<>(activity);
//        }
//
//        @Override
//        protected FeatureCollection doInBackground(Void... params) {
//            MapsActivity activity = activityRef.get();
//
//            if (activity == null) {
//                return null;
//            }
//
//            String geoJson = loadGeoJsonFromAsset(activity, "us_west_coast.geojson");
//            return FeatureCollection.fromJson(geoJson);
//        }
//
//        @Override
//        protected void onPostExecute(FeatureCollection featureCollection) {
//            super.onPostExecute(featureCollection);
//            MapsActivity activity = activityRef.get();
//            if (featureCollection == null || activity == null) {
//                return;
//            }
//
//// This example runs on the premise that each GeoJSON Feature has a "selected" property,
//// with a boolean value. If your data's Features don't have this boolean property,
//// add it to the FeatureCollection 's features with the following code:
//            for (Feature singleFeature : featureCollection.features()) {
//                singleFeature.addBooleanProperty(PROPERTY_SELECTED, false);
//            }
//
//            activity.setUpData(featureCollection);
//            new GenerateViewIconTask(activity).execute(featureCollection);
//        }
//
//        static String loadGeoJsonFromAsset(Context context, String filename) {
//            try {
//// Load GeoJSON file from local asset folder
//                InputStream is = context.getAssets().open(filename);
//                int size = is.available();
//                byte[] buffer = new byte[size];
//                is.read(buffer);
//                is.close();
//                return new String(buffer, Charset.forName("UTF-8"));
//            } catch (Exception exception) {
//                throw new RuntimeException(exception);
//            }
//        }
//    }


//    private static class GenerateViewIconTask extends AsyncTask<FeatureCollection, Void, HashMap<String, Bitmap>> {
//
//        private final HashMap<String, View> viewMap = new HashMap<>();
//        private final WeakReference<MapsActivity> activityRef;
//        private final boolean refreshSource;
//
//        GenerateViewIconTask(MapsActivity activity, boolean refreshSource) {
//            this.activityRef = new WeakReference<>(activity);
//            this.refreshSource = refreshSource;
//        }
//
//        GenerateViewIconTask(MapsActivity activity) {
//            this(activity, false);
//        }
//
//        @SuppressWarnings("WrongThread")
//        @Override
//        protected HashMap<String, Bitmap> doInBackground(FeatureCollection... params) {
//            MapsActivity activity = activityRef.get();
//            if (activity != null) {
//                HashMap<String, Bitmap> imagesMap = new HashMap<>();
//                LayoutInflater inflater = LayoutInflater.from(activity);
//
//                FeatureCollection featureCollection = params[0];
//
//                for (Feature feature : featureCollection.features()) {
//
//                    BubbleLayout bubbleLayout = (BubbleLayout)
//                            inflater.inflate(R.layout.symbol_layer_info_window_layout_callout, null);
//
//                    String name = feature.getStringProperty(PROPERTY_NAME);
//                    TextView titleTextView = bubbleLayout.findViewById(R.id.info_window_title);
//                    titleTextView.setText(name);
//
//                    String style = feature.getStringProperty(PROPERTY_CAPITAL);
//                    TextView descriptionTextView = bubbleLayout.findViewById(R.id.info_window_description);
//                    descriptionTextView.setText(
//                            String.format(activity.getString(R.string.capital), style));
//
//                    int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
//                    bubbleLayout.measure(measureSpec, measureSpec);
//
//                    float measuredWidth = bubbleLayout.getMeasuredWidth();
//
//                    bubbleLayout.setArrowPosition(measuredWidth / 2 - 5);
//
//                    Bitmap bitmap = SymbolGenerator.generate(bubbleLayout);
//                    imagesMap.put(name, bitmap);
//                    viewMap.put(name, bubbleLayout);
//                }
//
//                return imagesMap;
//            } else {
//                return null;
//            }
//        }
//
//        @Override
//        protected void onPostExecute(HashMap<String, Bitmap> bitmapHashMap) {
//            super.onPostExecute(bitmapHashMap);
//            MapsActivity activity = activityRef.get();
//            if (activity != null && bitmapHashMap != null) {
//                activity.setImageGenResults(bitmapHashMap);
//                if (refreshSource) {
//                    activity.refreshSource();
//                }
//            }
////            Toast.makeText(activity, R.string.tap_on_marker_instruction, Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    /**
//     * Utility class to generate Bitmaps for Symbol.
//     */
//    private static class SymbolGenerator {
//
//        /**
//         * Generate a Bitmap from an Android SDK View.
//         *
//         * @param view the View to be drawn to a Bitmap
//         * @return the generated bitmap
//         */
//        static Bitmap generate(@NonNull View view) {
//            int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
//            view.measure(measureSpec, measureSpec);
//
//            int measuredWidth = view.getMeasuredWidth();
//            int measuredHeight = view.getMeasuredHeight();
//
//            view.layout(0, 0, measuredWidth, measuredHeight);
//            Bitmap bitmap = Bitmap.createBitmap(measuredWidth, measuredHeight, Bitmap.Config.ARGB_8888);
//            bitmap.eraseColor(Color.TRANSPARENT);
//            Canvas canvas = new Canvas(bitmap);
//            view.draw(canvas);
//            return bitmap;
//        }
//    }
    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
// Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {

// Get an instance of the component
            LocationComponent locationComponent = mapboxMap.getLocationComponent();

// Activate with options
            locationComponent.activateLocationComponent(
                    LocationComponentActivationOptions.builder(this, loadedMapStyle).build());

// Enable to make component visible
            locationComponent.setLocationComponentEnabled(true);

// Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);

// Set the component's render mode
            locationComponent.setRenderMode(RenderMode.COMPASS);
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }


//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
//
//    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, "need", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            mapboxMap.getStyle(new Style.OnStyleLoaded() {
                @Override
                public void onStyleLoaded(@NonNull Style style) {
                    enableLocationComponent(style);
                }
            });
        } else {
            Toast.makeText(this, "no", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    @SuppressWarnings({"MissingPermission"})
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mapboxMap != null) {
            mapboxMap.removeOnMapClickListener(this);
        }
        mapView.onDestroy();
    }
}

//****************************************** Function Map ***************************************************************
//    @Override
//    public void onMapReady(final GoogleMap googleMap) {
//        mMap = googleMap;
//
////****************************************** set Max and Min Zoom Map ***************************************************************
//        googleMap.setMinZoomPreference(10.0f);
//        googleMap.setMaxZoomPreference(20.0f);
//
////****************************************** Listener Click on Markers ***************************************************************
//        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//            @Override
//            public boolean onMarkerClick(final Marker marker) {
//
//                if (marker.getTitle() != null) {
//                    optionDialog = new AlertDialog.Builder(MapsActivity.this).create();
//                    ImageView imageView;
//                    TextView text_name, text_address;
//                    Button btn_go;
//                    final View view = getLayoutInflater().inflate(R.layout.dialog_show_detail, null);
//                    optionDialog.setView(view);
//
//                    imageView = view.findViewById(R.id.image_place);
//                    text_name = view.findViewById(R.id.text_name);
//                    text_address = view.findViewById(R.id.text_address);
//                    btn_go = view.findViewById(R.id.btn_go);
//
//                    text_name.setText(marker.getTitle());
//                    text_address.setText(marker.getSnippet());
//                    imageView.setImageResource((int) marker.getTag());
//
//
//                    btn_go.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            // TODO: 12/20/19 direction
//
//                            List<Marker> listMarkers = new ArrayList<>();
//                            List<LatLng> listLatLngs = new ArrayList<>();
//
//                            if (currentLatLng == null) {
//                                Toast.makeText(MapsActivity.this, "لوکیشن خود را بررسی کنید", Toast.LENGTH_SHORT).show();
//                                return;
//                            }
//
//                            markerOptions = new MarkerOptions().position(currentLatLng);
//                            markerCurrent = googleMap.addMarker(markerOptions);
//
//                            listLatLngs.add(marker.getPosition());
//                            listLatLngs.add(markerOptions.getPosition());
//                            listMarkers.add(marker);
//                            listMarkers.add(markerCurrent);
//                            Polyline polyline = null;
//                            if (polyline != null)
//                                polyline.remove();
//
//
//                            googleMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
//
//                            PolylineOptions polylineOptions = new PolylineOptions().addAll(listLatLngs).clickable(true);
//                            googleMap.addPolyline(polylineOptions);
//
//
////                            GoogleDirection.withServerKey("AIzaSyCliyo6n938nEvYG63UeDrolHFA4iOgIaI")
////                                    .from(currentLatLng)
////                                    .to(marker.getPosition())
////                                    .avoid(AvoidType.FERRIES)
////                                    .avoid(AvoidType.HIGHWAYS)
////                                    .execute(new DirectionCallback() {
////                                        @Override
////                                        public void onDirectionSuccess(Direction direction) {
////                                            if(direction.isOK()) {
////                                                // Do something
////                                                Toast.makeText(MapsActivity.this, "ok", Toast.LENGTH_SHORT).show();
////
////                                            } else {
////                                                // Do something
////                                                Toast.makeText(MapsActivity.this, "no", Toast.LENGTH_SHORT).show();
////
////                                            }
////                                        }
////
////                                        @Override
////                                        public void onDirectionFailure(Throwable t) {
////                                            // Do something
////                                            Toast.makeText(MapsActivity.this, "failure", Toast.LENGTH_SHORT).show();
////                                        }
////                                    });
//
////                            googleMap.addMarker(new MarkerOptions()
////                                    .position(new LatLng(currentLatLng.latitude, currentLatLng.longitude))
////                                    .title("مبدا")
////                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
////
////                            // Getting URL to the Google Directions API
////                            String url = getDirectionsUrl(currentLatLng, marker.getPosition());
////
////                            DownloadTask downloadTask = new DownloadTask();
////
////                            // Start downloading json data from Google Directions API
////                            downloadTask.execute(url);
////                                    new FetchURL(MapsActivity.this).execute(getUrl(currentLatLng, marker.getPosition(), "driving"), "driving");
//                            optionDialog.dismiss();
//                        }
//                    });
//
//
//                    optionDialog.show();
//                }
//
//                return false;
//            }
//        });
//
//
//        btn_cinema.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                googleMap.clear();
//                for (int i = 0; i < cinema.size(); i++) {
//                    Marker marker =
//                            googleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(cinema.get(i).lat, cinema.get(i).lng))
//                                    .title(cinema.get(i).tag)
//                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
//                                    .snippet(cinema.get(i).address));
//                    marker.setTag(cinema.get(i).image);
//
//                }
//                googleMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(cinema.get(0).lat, cinema.get(0).lng)));
//                bottomSheetFragment = new BottomSheetFragment(cinema);
//                //noinspection deprecation
//                bottomSheetFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme);
//                bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
//            }
//        });
//        btn_cafe.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                googleMap.clear();
//                for (int i = 0; i < cafe.size(); i++) {
//                    Marker marker =
//                            googleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(cafe.get(i).lat, cafe.get(i).lng))
//                                    .title(cafe.get(i).tag)
//                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
//                                    .snippet(cafe.get(i).address));
//                    marker.setTag(cafe.get(i).image);
//                }
//
//                googleMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(cafe.get(0).lat, cafe.get(0).lng)));
//                bottomSheetFragment = new BottomSheetFragment(cafe);
//                //noinspection deprecation
//                bottomSheetFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme);
//                bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
//            }
//        });
//
//        btn_restaurant.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                googleMap.clear();
//                for (int i = 0; i < restaurant.size(); i++) {
//                    Marker marker =
//                            googleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(restaurant.get(i).lat, restaurant.get(i).lng))
//                                    .title(restaurant.get(i).tag)
//                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
//                                    .snippet(restaurant.get(i).address));
//                    marker.setTag(restaurant.get(i).image);
//                }
//                googleMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(restaurant.get(0).lat, restaurant.get(0).lng)));
//                bottomSheetFragment = new BottomSheetFragment(restaurant);
//                //noinspection deprecation
//                bottomSheetFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme);
//                bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
//            }
//        });
//        btn_park.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                googleMap.clear();
//                for (int i = 0; i < park.size(); i++) {
//                    Marker marker =
//                            googleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(park.get(i).lat, park.get(i).lng))
//                                    .title(park.get(i).tag)
//                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
//                                    .snippet(park.get(i).address));
//                    marker.setTag(park.get(i).image);
//                }
//                googleMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(park.get(0).lat, park.get(0).lng)));
//                bottomSheetFragment = new BottomSheetFragment(park);
//                //noinspection deprecation
//                bottomSheetFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme);
//                bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
//            }
//        });
//
//
//        LatLng sydney = new LatLng(36.264511, 50.015844);
//        googleMap.animateCamera(CameraUpdateFactory.newLatLng(sydney));
//    }


//private class DownloadTask extends AsyncTask<String, Void, String> {
//
//    @Override
//    protected String doInBackground(String... url) {
//
//        String data = "";
//
//        try {
//            data = downloadUrl(url[0]);
//        } catch (Exception e) {
//            Log.d("Background Task", e.toString());
//        }
//        return data;
//    }
//
//    @Override
//    protected void onPostExecute(String result) {
//        super.onPostExecute(result);
//
//        ParserTask parserTask = new ParserTask();
//
//
//        parserTask.execute(result);
//
//    }
//}
//
//
///**
// * A class to parse the JSON format
// */
//private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
//
//    // Parsing the data in non-ui thread
//    @Override
//    protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
//
//        JSONObject jObject;
//        List<List<HashMap<String, String>>> routes = null;
//
//        try {
//            jObject = new JSONObject(jsonData[0]);
//            DataParser parser = new DataParser();
//
//            routes = parser.parse(jObject);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return routes;
//    }
//
//    @Override
//    protected void onPostExecute(List<List<HashMap<String, String>>> result) {
//        ArrayList points = new ArrayList();
//        PolylineOptions lineOptions = new PolylineOptions();
//
//        for (int i = 0; i < result.size(); i++) {
//
//            List<HashMap<String, String>> path = result.get(i);
//
//            for (int j = 0; j < path.size(); j++) {
//                HashMap<String, String> point = path.get(j);
//
//                double lat = Double.parseDouble(point.get("lat"));
//                double lng = Double.parseDouble(point.get("lng"));
//                LatLng position = new LatLng(lat, lng);
//
//                points.add(position);
//            }
//
//            lineOptions.addAll(points);
//            lineOptions.width(12);
//            lineOptions.color(Color.RED);
//            lineOptions.geodesic(true);
//
//        }
//
//// Drawing polyline in the Google Map for the i-th route
//
//        if (points.size() != 0)
//            mMap.addPolyline(lineOptions);
//    }
//
//}
//
//    private String getDirectionsUrl(LatLng origin, LatLng dest) {
//
//        // Origin of route
//        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
//
//        // Destination of route
//        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
//
//        //setting transportation mode
//        String mode = "mode=driving";
//
//        // Building the parameters to the web service
//        String parameters = str_origin + "&" + str_dest + "&" + mode;
//
//        // Output format
//        String output = "json";
//
//        // Building the url to the web service
//        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + "AIzaSyCliyo6n938nEvYG63UeDrolHFA4iOgIaI";
//
//
//        return url;
//    }
//
//    /**
//     * A method to download json data from url
//     */
//    private String downloadUrl(String strUrl) throws IOException {
//        String data = "";
//        InputStream iStream = null;
//        HttpURLConnection urlConnection = null;
//        try {
//            URL url = new URL(strUrl);
//
//            urlConnection = (HttpURLConnection) url.openConnection();
//
//            urlConnection.connect();
//
//            iStream = urlConnection.getInputStream();
//
//            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
//
//            StringBuffer sb = new StringBuffer();
//
//            String line = "";
//            while ((line = br.readLine()) != null) {
//                sb.append(line);
//            }
//
//            data = sb.toString();
//
//            br.close();
//
//        } catch (Exception e) {
//            Log.d("Exception", e.toString());
//        } finally {
//            iStream.close();
//            urlConnection.disconnect();
//        }
//        return data;
//    }
//
//}