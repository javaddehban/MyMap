package app.load.mymap;

public class Markers {
    public int id;
    double lat;
    double lng;
    int star;
    int image;
    String tag;
    String address;

    Markers(int id, double lat, double lng, String tag, String address, int star, int image) {
        this.id = id;
        this.lat = lat;
        this.lng = lng;
        this.tag = tag;
        this.address = address;
        this.star = star;
        this.image = image;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
